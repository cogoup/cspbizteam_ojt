package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;

//비즈니스 로직 처리

@Service
public class MemberService {
	Logger logger = LoggerFactory.getLogger(MemberService.class);
	@Autowired
	private UserRepository repo;

	public int insertMember(User domain) {
		if (repo.findById(domain.getId()).orElse(null) != null) {
			logger.info("해당 데이터 존재");
			return 1;
		}
		try {
			repo.save(domain);
		} catch (Exception e) {
			// 쿼리 과정 중 에러 발생
			return 2;
		}
		return 0;
	}

	public List<User> selectMember() {
		List<User> tf = (List<User>) repo.findAll();
		return tf;
	}

	public int updateMember(User domain) {
		if (repo.findById(domain.getId()).orElse(null) == null) {
			// 존재하지 않는 데이터
			return 1;
		}
		try {
			repo.save(domain);
		} catch (Exception e) {
			// 쿼리 과정 중 에러 발생
			return 2;
		}
		return 0;

	}

	public void deleteMember(String id) {
		repo.deleteById(id);
	}

}
