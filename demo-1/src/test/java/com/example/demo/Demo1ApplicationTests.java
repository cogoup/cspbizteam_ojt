package com.example.demo;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;

@SpringBootTest
class Demo1ApplicationTests {
	Logger logger = LoggerFactory.getLogger(Demo1ApplicationTests.class);
	@Autowired
	UserRepository repo;

	@Test
	void TestCRUD() {
		User newDomain = new User("abcdef", "a1234");
		User loadDomain;

		// 입력
		repo.save(newDomain);
		// 조회
		loadDomain = repo.findById("abcdef").orElse(null);
		if (loadDomain == null)
			logger.info("NOT INSERTED");
		else
			logger.info("INSERTED : " + loadDomain.getId());
		// 수정
		newDomain.setPassword("1213");
		repo.save(newDomain);

		// 삭제
		repo.deleteById("abcdef");
	}

}
