package kr.co.opensource.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitConsumer {
	private Logger logger = LoggerFactory.getLogger(RabbitConsumer.class);
	
	@RabbitListener(queues = "${spring.rabbitmq.template.default-receive-queue}")
	public void receiveMsg(Message msg) {
		logger.info("=== NEW MESSAGE RECEIVED.");
		logger.info(msg.toString());
		logger.info("=========================");
	}
}
