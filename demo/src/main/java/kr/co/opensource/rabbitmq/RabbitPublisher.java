package kr.co.opensource.rabbitmq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RabbitPublisher {
	private static final String EXCHANGE_NAME = "test.exchange";
	@Autowired RabbitTemplate rabbitTemplate;

	@GetMapping("/sample/queue")
	public String samplePublish() {
		rabbitTemplate.convertAndSend(EXCHANGE_NAME, "test.key", "RabbitMQ + Springboot = Success!");
		return "message sending!";
	}

}
