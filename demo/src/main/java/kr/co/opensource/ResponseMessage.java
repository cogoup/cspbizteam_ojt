package kr.co.opensource;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ResponseMessage {
	private HttpStatus status;
	private String message;
	private Object data;
}
