package com.example.demo.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaSubscriber {
	
	private final Logger logger = LoggerFactory.getLogger(KafkaSubscriber.class);
	private final String topic = "test_topic";
	
	@KafkaListener(topics = topic)
	public void recieveMessage(String msg) {
		logger.info("=== NEW MESSAGE RECEIVED.");
		logger.info(msg);
		logger.info("=========================");
	}
}
