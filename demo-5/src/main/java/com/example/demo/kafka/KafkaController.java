package com.example.demo.kafka;
import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.ResponseMessage;


@RestController
public class KafkaController {

	@Autowired KafkaPublisher publisher;
	
	@PostMapping("/kafka")
	public ResponseEntity publish(@RequestBody(required=true) String message) {

		ResponseMessage msg = new ResponseMessage();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));
		
		boolean result = publisher.publish(message);
		
		if(!result) {
			msg.setStatus(HttpStatus.BAD_REQUEST);
			msg.setMessage("UNKNOWN ERROR OCCURED");
			return new ResponseEntity<ResponseMessage>(msg, headers, HttpStatus.METHOD_NOT_ALLOWED);
		}
		else {
			msg.setStatus(HttpStatus.OK);
			msg.setMessage("OK");
			msg.setData(message);
			return new ResponseEntity<ResponseMessage>(msg, headers, HttpStatus.OK);
		}
		
	}
}
