package com.example.demo.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Service
public class KafkaPublisher {
	
	@Autowired private KafkaTemplate<String, String> kafkaTemplate;
	private final String topic = "test_topic";
	private Logger logger = LoggerFactory.getLogger(KafkaPublisher.class);
	
	public boolean publish(String msg) {
		try {
			ListenableFuture<SendResult<String, String>> listenable = kafkaTemplate.send(topic, msg);
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}
	
}
