## :closed_book: 로컬 개발 환경 세팅 순서!

1. [java jdk](https://www.oracle.com/java/technologies/downloads/) ver 8 install
    - STS, Eclipse, InteliJ tool
    - Environment variables Setting
    - Springboot Framework
    - Create Project : [String Initialzr](https://start.spring.io/) 
                       File > Spring Starter Project > java version 8 > select dependencies > finish
    - install jdk in docker container
    - right-click project > Run As > Spring Boot App 
                    
```
yum list | grep jdk
yum install java-1.8.0-openjdk-devel.x86_64
which javac
readlink -f $(which javac)
vim /etc profile

export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.332.b09-1.el7_9.x86_64/bin/javac

PATH=$PATH:$JAVA_HOME/bin
echo $JAVA_HOME
source etc/profile

```
2. [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/) install
    - install mariadb and mongodb in docker container
```
yum list | grep docker
yum install yum-utils
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

yum install docker-ce

systemctl enable docker
systemctl start docker
docker ps -a
```

3. [Git Bash](https://git-scm.com/) install
```
cd {project name}
git config --global user.name "username"
git config --global user.email "useremail"
git config --list

git init
git remote add origin "https://gitlab.com/cogoup/cspbizteam_ojt.git"
git add .
git commit -m "coment"
git push -u  origin master
```

4. DB
- MariaDB install
   [Heidsql](https://www.heidisql.com/download.php) insatll

```
docker pull mariadb
docker run \
-p 3306:3306 \
-e MYSQL_ROOT_PASSWORD=password \
-e MYSQL_ROOT_HOST='%' \
--restart=always \
--name=mariadb \
-d \
mariadb \
--character-set-server=utf8mb4 \
--collation-server=utf8mb4_unicode_ci
```

- MongoDB install
   [MongoCompass](https://www.mongodb.com/ko-kr/products/compass) install

```
docker pull mongo
 docker run \
--name=mongo \
-p 27017:27017 \
--restart=always \
-e TZ=Asia/Seoul \
-e MONGO_INITDB_ROOT_USERNAME=root \
-e MONGO_INITDB_ROOT_PASSWORD=password \
-d \
mongo
```

- Redis install
   [P3X](https://www.electronjs.org/apps/p3x-redis-ui) install

```
docker run \
--name redis \
-p 6379:6379 \
-d \
redis
```

## application.properties setting

<pre>
<code>
# MariaDB
spring.datasource.url=jdbc:mysql://{ip_address}:{port_number}/{db_name}?allowMultiQueries=true&useOldAliasMetad`ataBehavior=true&useSSL=false
spring.datasource.username={username}
spring.datasource.password={password}
spring.datasource.driverClassName=org.mariadb.jdbc.Driver

# JPA
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=update
logging.level.org.hibernate.type.descriptor.sql=trace

# Thymeleaf
spring.thymeleaf.cache=false
spring.thymeleaf.prefix=classpath:templates/
spring.thymeleaf.suffix=.html

server.port=8082

# MongoDB
spring.data.mongodb.host={ip_address}
spring.data.mongodb.port=27017
spring.data.mongodb.database={db_name}
spring.data.mongodb.authentication-database=admin
spring.data.mongodb.username={usernmae}
spring.data.mongodb.password={password}
spring.main.allow-bean-definition-overriding=true

# redis
spring.redis.host={ip_address}
spring.redis.port={port_number}

# rabbitmq
spring.rabbitmq.host={ip_number}
spring.rabbitmq.port={port_number}
spring.rabbitmq.username={username}
spring.rabbitmq.password={password}

spring.rabbitmq.template.exchange={exchange_name}
spring.rabbitmq.template.default-receive-queue={queue_name}
spring.rabbitmq.template.routing-key={key_name}

# kafka
spring.kafka.bootstrap-servers={ip_number}
spring.kafka.consumer.group-id=test
</code>
</pre>

5. deploy
[winSCP](https://winscp.net/eng/download.php) install 
right-click a file > Run As > Maven clean > Maven Build.. > Enter package in Goals -> Run
target file -> jar file 

```
cd / 
cd app
java -jar -Dserver.port=8881 -Dserver.address=0.0.0.0 demo-1-0.0.1-SNAPSHOT.jar
```

6. rabbitmq
[rabbitmqm anagement](http://abh0518.net/tok/?p=384) Reference

```
docker pull rabbitmq:management

docker run \
-d \
-p 5672:5672 \
-p 15672:15672 \
--restart=always \
rabbitmq:management
```

7. HAProxy

```
yum install haproxy
vim /etc/haproxy/haproxy.cfg

frontend test_page
        bind *:{port}
        default_backend test_servers
        option forwardfor

backend test_servers
        balance roundrobin
        server web1 localhost:9998
        server web3 localhost:9797

frontend stats
        bind *:8404
        stats enable
        stats uri /stats
        stats refresh 10s
        stats admin if LOCALHOST

listen rabbitmq {ip:port}
        mode    tcp
        stats   enable
        balance roundrobin
        option  tcplog
        no option       clitcpka
        no option       srvtcpka
        server rabbit01 {ip:rabbitmq port}
        
 while true ; do nc -l -p 9998 -c 'echo -e "HTTP/1.1 200 OK\n\n 9998"'; done &
 while true ; do nc -l -p 9797 -c 'echo -e "HTTP/1.1 200 OK\n\n 9797"'; done &
 
 setsebool -P haproxy_connext_any=1
 netstat -tnlp | grep 9996
 
 systemctl enable haproxy
 systemctl start haproxy
 systemctl status haproxy
```

8. TDD(Test-Driven Development) Junit 

9. select IoT Topic

10. README.md [Markdown](https://gist.github.com/ihoneymon/652be052a0727ad59601), [emoji](https://gist.github.com/rxaviers/7360908)


<br>

💿 장비설치 절차
   1) 동봉 된 USB에 CentOS 7 ISO파일로 부팅디스크 제작
        - CentOS 7 Download : https://mirror.kakao.com/centos/7.9.2009/isos/x86_64/
        - USB 부팅디스크 제작 : https://rufus.ie/ko/
   2) 거치대 조립 후 본체 설치
   3) 전원 어댑터 및 모니터/마우스/키보드 연결
   4) USB 부팅디스크 연결 후 전원 인가
   5) 부팅순서를 USB로 해두고 부팅 시작
   6) CentOS 7 설치 (GUI 모드로 설치)
   7) N-MAS 설치 진행
 </br>

