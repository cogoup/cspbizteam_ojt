package com.example.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.domain.User;
import com.example.demo.service.MemberService;

@Controller
public class TestController {
	Logger logger = LoggerFactory.getLogger(TestController.class);
	@Autowired
	private MemberService service;

	@GetMapping("/")
	public String test() {
		return "index";
	}

	@RequestMapping(value = "/insert")
	public String insert() {
		return "/member/insertMember";
	}

	@RequestMapping(value = "/select")
	public String select() {
		return "/member/selectMember";
	}

	// create
	@PostMapping("/insertMember")
	public String member(@RequestBody User form) {
		service.insertMember(form);
		return "redirect:/";
	}

	@GetMapping("/selectMember")
	@ResponseBody
	public List<User> selectMember(Model model) {
		List<User> memberList = service.selectMember();
		return memberList;
	}

	// update
	@PostMapping("/updateMember")
	public String updateMember(@RequestBody User form) {
		service.updateMember(form);
		return "redirect:/";
	}

	// delete
	@GetMapping("/deleteMember")
	public String deleteMember(String id) {
		service.deleteMember(id);
		return "redirect:/";
	}
}
