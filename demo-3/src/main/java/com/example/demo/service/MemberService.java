package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import com.example.demo.controller.TestController;
import com.example.demo.domain.User;

@Component
public class MemberService {
	Logger logger = LoggerFactory.getLogger(TestController.class);
	@Autowired
	private StringRedisTemplate redisTemplate;
	private ValueOperations<String, String> stringOps;
	private List<String> list = new ArrayList<>();
	

	public void insertMember(User form) {
		stringOps = this.redisTemplate.opsForValue();
		stringOps.set(form.getId(), form.getPassword());
		list.add(form.getId());
	}

	public List<User> selectMember() {
		List<User> uList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			String val = stringOps.get(list.get(i));
			uList.add(new User(list.get(i), val));
		}
		return uList;
	}

	public void updateMember(User form) {
		stringOps.set(form.getId(), form.getPassword());
	}

	public void deleteMember(String id) {
		stringOps.getAndDelete(id);			
	}

}
