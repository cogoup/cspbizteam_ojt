package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.MongoDomain;

@Repository
public interface abcd extends MongoRepository<MongoDomain, String> {
	public Optional<MongoDomain> findById(String id);
	public void deleteById(String id);
	public List<MongoDomain> findAll();
	public MongoDomain insert(MongoDomain md);

}
